const employeesData = [
  {
    id: 1,
    firstName: 'saurabh',
    lastName: 'sharma',
    email: 'saurabh@example.com',
    salary: '95000',
    date: '2019-04-11',
  },
  {
    id: 2,
    firstName: 'rohit',
    lastName: 'kumar',
    email: 'rohit@example.com',
    salary: '80000',
    date: '2019-04-17',
  },
  {
    id: 3,
    firstName: 'amit',
    lastName: 'rao',
    email: 'amit@example.com',
    salary: '79000',
    date: '2019-05-01',
  },
  {
    id: 4,
    firstName: 'vikash',
    lastName: 'sharma',
    email: 'vikash@example.com',
    salary: '56000',
    date: '2019-05-03',
  },
  {
    id: 5,
    firstName: 'ashsih',
    lastName: 'rawat',
    email: 'ashsih@example.com',
    salary: '65000',
    date: '2019-06-13',
  },
  {
    id: 6,
    firstName: 'isha',
    lastName: 'gupta',
    email: 'isha@example.com',
    salary: '120000',
    date: '2019-07-30',
  },
];

export { employeesData };
