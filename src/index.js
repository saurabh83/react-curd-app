import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './components/App';
import Layout from './components/Layout/index'
import Header from './components/Layout/Header'


const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <React.StrictMode>
    <Header/>
    <App />
    <Layout/>
  </React.StrictMode>
);
